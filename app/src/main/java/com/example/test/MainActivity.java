package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;


import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.uber.sdk.rides.client.ServerTokenSession;


public class MainActivity<config, requestButton> extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    SessionConfiguration config = new SessionConfiguration.Builder()
            // mandatory
            .setClientId("rKcW9oQTmOnOv4Ld5JyFxQBEO__7P6CW")
            // required for enhanced button features
            .setServerToken("CAESELq6cVrC5EGzuPdrBQwCEKYiATE.8svI0_eRxyL3Z2Kzd1xTl1ovcO9v6rFI0I7skdUXC0g&state=dKn0T1MSUvghhvY64Jw2KsmDtT1QufI7r8wX0Y7IfwE%3D#_")
            // required for implicit grant authentication
            .setRedirectUri("")
            // optional: set sandbox as operating environment
            //.setScopes(Arrays.asList(Scope.RIDE_WIDGETS))
            .setEnvironment(SessionConfiguration.Environment.PRODUCTION)
            .build();

    UberSdk.initialize(config);

    // get the context by invoking ``getApplicationContext()``, ``getContext()``, ``getBaseContext()`` or ``this`` when in the activity class
   // RideRequestButton requestButton = new RideRequestButton(MainActivity.this);
    // get your layout, for instance:
    //RelativeLayout layout = (RelativeLayout) findViewById(R.id.activity_main);
    //RelativeLayout layout = new RelativeLayout(this);
    //layout.addView(requestButton);

}